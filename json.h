//A simple little JSON Interpreter, Doesn't work with unicode (utf-8 maybe?) so dont bother, kay?

#include <string>
#include <vector>
#include <map>

namespace JSON{

//forward declares
	class JSON_Container;

//Type definitions
	typedef std::string JString;
	typedef float JNumber;
	typedef std::vector<JSON_Container * > JArray;
	typedef std::map<JString, JSON_Container * > JObject;
	typedef char JChar;
	typedef char JBool;

//Type Enumeration
	//a list of acceptable types, duh.
	enum JSON_Type{
		JSON_ERROR = 0,
		JSON_STRING,
		JSON_NUMBER,
		JSON_ARRAY,
		JSON_OBJECT,
		JSON_BOOL,
		JSON_NULL,
		
	};


//======
//JSON_Container
//Interface and general data storage class
//======
class JSON_Container{
	public:
	//Constructors
		JSON_Container();
		~JSON_Container();
		JSON_Container(JSON_Container& Existing);
		//
			JSON_Container(JString& String);
			JSON_Container(JNumber& Number);
			JSON_Container(JBool & Bool);
			
	//Data Retrievers for each type.
		JString 			GetString();
		JNumber 			GetNumber();
		JBool 				GetBool();
		JSON_Container * 	GetElement(unsigned int Element);
		JSON_Container * 	GetMember(JString Key);
	//Type Retrieval 
		JSON_Type GetType();
	
	//Setters
		void SetType(JSON_Type type);
		void SetString(JString String);
		void SetNumber(JNumber Number);
		void SetBool(JBool Value);
		void SetNull();
		
		void AddMember(JString Key,JSON_Container * Value);
		void RemoveMember(JString key);
	
		void AddElement(JSON_Container * Value);
		void RemoveElement(unsigned int Position);
		unsigned int ElementCount();
		
	//Stringify -- returns contained json data as string
		JString Stringify();
	//---
	private:
	//Type
		JSON_Type mType; 
	//Data Storage
		JString		mString;
		JNumber		mNumber;
		JArray		mArray;
		JObject		mObject;
		JBool 		mBool;
};

//======
// The Primary Interface
//======
	//Load JSON from a string NOTE: consumes string
		JSON_Container * Parse(std::string & Jason);
	//Load JSON from a file
		JSON_Container * LoadFromFile(std::string FilePath);
//======
// The Internal Interface
//======
namespace Tools{
	//trims whitespace
		bool TrimWhiteSpace(std::string & json);
	//parses a string value
		bool ReadString(std::string & json, std::string & destination);
	//Parses a number value
		bool ReadNumber(std::string & json, JNumber & destination);
	//Parses a Boolean Value
		bool ReadBool(std::string & json, JBool & destination);
	//Parses a Null value
		bool ReadNull(std::string & json);
	//safely remove characters from a string (if there are any).
		bool RemoveChar(std::string & json, unsigned int count);
	//is a number?
		bool IsDigit(JChar Test );
};

 

};

