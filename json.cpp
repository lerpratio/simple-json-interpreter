//stl includes
#include <iostream>
#include <cmath>
#include <sstream>

//local includes
#include "json.h"
//usings
	using std::string;
	using std::vector;
	using std::map;

	using std::cout;
	
namespace JSON{
//=====
//JSON_Container implementation
//=====
	JSON_Container::JSON_Container(){
		mType = JSON_NULL;
	};
	//--
	JSON_Container::~JSON_Container(){
		//free array
		//free objects
	};
	//--
	JSON_Container::JSON_Container(JSON_Container& Existing){
	//copy data/arrays/children
	};
	
	
	JSON_Container::JSON_Container(JString& String){
		mType = JSON_STRING;
		mString = String;
	};
	
	JSON_Container::JSON_Container(JNumber& Number){
		mType = JSON_NUMBER;
		mNumber = Number;
		//
		//mString = String;
	};
	
	JSON_Container::JSON_Container(JBool& Input){
		mType = JSON_BOOL;
		mBool = Input;
		//
		//mString = String;
	};
	
	//Data Retrievers for each type.
	JString JSON_Container::GetString(){
		return mString;
	};
	JNumber JSON_Container::GetNumber(){
		return mNumber;
	};
	JBool JSON_Container::GetBool(){
		return mBool;
	};

	JSON_Container * JSON_Container::GetElement(unsigned int Element){
		//bounds check
		if ( (Element < 0) || (Element > mArray.size()-1) ){
			return 0;
		} 

		return mArray.at(Element);
	};

	//JSON_Container * JSON_Container::GetMember(JString Member){};
	
	JSON_Type JSON_Container::GetType(){
		return mType ;
	};

	void JSON_Container::SetType(JSON_Type Type){
		mType =  Type;
	};
	void JSON_Container::SetString(JString String){
		mString =  String;
	};
	void JSON_Container::SetNumber(JNumber Number){
		mNumber = Number;
	};
	void JSON_Container::SetBool(JBool Value){
		mBool = Value;
	};
	void JSON_Container::SetNull(){
		mType = JSON_NULL;
	};
	
	void JSON_Container::AddMember(JString Key,JSON_Container * Value){

	};
	void JSON_Container::AddElement(JSON_Container * Value){
		mArray.push_back(Value);
	};

	void JSON_Container::RemoveMember(JString key){

	};

	void JSON_Container::RemoveElement(unsigned int Position){
		if ( (Position+1 > mArray.size()) || Position<= 0)  {
			return;
		}

		mArray.erase(mArray.begin() + Position);
	};

	unsigned int JSON_Container::ElementCount(){
		return mArray.size();
	};
//Stringify -- returns contained json data as string
	JString JSON_Container::Stringify(){
		std::stringstream output;
		switch(mType){
			case JSON_NULL:
				output << "null";
				return output.str();

			case JSON_BOOL:
				if (GetBool()){
					output << "true";
				} else {
					output << "false";
				}
				return output.str();

			case JSON_NUMBER:
				output.precision(12);
				output << mNumber;
				return output.str();

			case JSON_STRING:
				output << "\"";			
				output << mString;
				output << "\"";
				return output.str();

			case JSON_ARRAY:
				output << "[";
				for (int i = 0; i < ElementCount(); ++i)
				{
					output << GetElement(i)->Stringify();

					if (i != (ElementCount()-1) ){
						output << ",";	
					}
				}
				output << "]";
				return output.str();

			case JSON_OBJECT:
				output << "null";
				return output.str();
				
			default: 
				output << "[error]";
				return output.str();
		}	

	};

	
//=====
//Interaface implementation
//=====
JSON_Container* Parse(std::string& json){
	bool error = 0;	
	JSON_Container * result;
	//loop start
	//while ((json.size()) && (!error)){
		Tools::TrimWhiteSpace(json); 
		//cout << json[0];
		
	//is a number?
		//check for [0-9],[-],[e],[E],[.]
			if(Tools::IsDigit(json[0])){
				JSON_Container * jtempnum  = 0;
				float receiver = 0;
				
				//cout << "Digit detected.\n";
				if ( !Tools::ReadNumber(json, receiver) ){
					//error
					cout << "num read error.\n";
					return 0;	
				}
				//cout << "Digits read.\n";
				//cout.precision(12);
			//parse then post
				jtempnum = new JSON_Container(receiver);
				cout << jtempnum->GetNumber() << "\n";	
				return jtempnum;
				
			}
		
	//is a string? 
		//check for ["]
			if (json[0] == '"'){
				JSON_Container * jtempstr = 0;
				std::string outputstr;
		//read a string
				if ( !Tools::ReadString(json,outputstr) ){
					if (jtempstr) delete jtempstr;
					//return error
					cout << "string read error.\n";
					return 0;
				}
				jtempstr = new JSON_Container(outputstr);
				//cout << jtempstr->GetString() << "\n";
				return jtempstr;
			}
		//trim white space then post
	//is an array?
			if (json[0] == '['){
				JSON_Container * jtemparray = 0;
				Tools::RemoveChar(json,1);
				Tools::TrimWhiteSpace(json); 

				if (json[0] == ']'){
					//empty array
					jtemparray = new JSON_Container();
					jtemparray->SetType(JSON_ARRAY);
					Tools::RemoveChar(json,1);
					
					return jtemparray;

				}
				
				//we got an array!
				jtemparray = new JSON_Container();
				jtemparray->SetType(JSON_ARRAY);
				
				JSON_Container * jtemparraychild = Parse(json);
				if (!jtemparraychild){
						cout << "error parsing array\n";
						if (jtemparray) delete jtemparray;
						return 0;
					}
				Tools::TrimWhiteSpace(json); 
				jtemparray->AddElement(jtemparraychild);
					

				while (json[0] == ','){
					Tools::RemoveChar(json,1); 
					//read a new value
					JSON_Container * jtemparraychild = Parse(json);
					if (!jtemparraychild){
						cout << "error parsing array\n";
						if (jtemparray) delete jtemparray;
						return 0;
					}

					Tools::TrimWhiteSpace(json); 
					jtemparray->AddElement(jtemparraychild);

				};

				if (json[0] == ']'){
					//finish array
					Tools::RemoveChar(json,1);
					cout << "array over \n"; 	
					cout << jtemparray->Stringify()<< "\n"; 
					return jtemparray;

				} else {
						cout << "error parsing array\n";
						if (jtemparray) delete jtemparray;
						return 0;
				}


			}

	//is an object?	
		//is empty? break.
		//process ID string. if invalid, error
		//next is a [:] or error
		//process value. if invalid error.
		//more elements? loop
		//no closing brackets? error

			if (json[0] == '{'){
				//object
				JSON_Container * jtempobject = 0;
				string desty = "";

				jtempobject = new JSON_Container;
				jtempobject->SetType(JSON_OBJECT);

				if (!Tools::ReadString(json, desty)){
					cout << "error parsing object: Key \n";
					if (jtempobject) delete jtempobject;
					return 0;
				};

				Tools::TrimWhiteSpace(json);
				
				if (json[0] == ':'){
					Tools::RemoveChar(json,1);
				} else {
					cout << "error parsing object: ':' \n";
					if (jtempobject) delete jtempobject;
					return 0;
				}

				Tools::TrimWhiteSpace(json);

				JSON_Container * jtempobjectchild = Parse(json);
				Tools::TrimWhiteSpace(json);
				
				while(json[0] == ","){

				}
				


			}
	//is boolean?
		//check for t/f
			if (json[0] == 't' || json[0] == 'f'){
				JSON_Container * jtempbool = 0;
				JBool  boolrec = 0;

				if ( !Tools::ReadBool(json, boolrec ) ){
					cout << "bool read error\n";
					if (jtempbool) delete jtempbool;
					return 0;
				}
				jtempbool = new JSON_Container();
				jtempbool->SetType(JSON_BOOL);
				jtempbool->SetBool(boolrec);

				if ( jtempbool->GetBool() ) {
					cout << "true";
				} else {
					cout << "false";
				}

				return jtempbool;
			}
	//is null
		if (json[0] == 'n'){
			JSON_Container * jtempnull = 0;
			if (!Tools::ReadNull(json) ){
				cout <<  "null read error";
				return 0;
			}

			jtempnull = new JSON_Container();
			jtempnull->SetType(JSON_NULL);
			if (jtempnull->GetType() == JSON_NULL){
				cout << "NULL!\n";
			} 
			return jtempnull; //new JSON_Container;


		}
	//default case
		//report error
			cout << "Error Parsing JSON.\n";
			return 0;
	//}
	
}
//
	
//=====
//Internal interface stuff
//=====
namespace Tools{
//---
	bool TrimWhiteSpace(std::string & json){
		bool result = true;
		
		while ( (json[0] == ' ') || (json[0] == '\t') || (json[0] == '\r') || (json[0] == '\n') ){
			//if EOF, error!
			if (json.size() == 0){
				result = false;
				break;
			}
			// if whitspace trim
			json = json.substr(1, json.size()-1);			
		}
		
		return result;		
	}
//---
	bool ReadString(std::string & json, std::string & destination){
		bool result = false;
		destination = "";
		//cout << "reading string\n";
		TrimWhiteSpace(json);
		
		//has a "?
		//cout << "testing letter.. \n";
		if (json[0] == '"'){
			//cout << "String Initialized\n";
			//clip 1 letter
			RemoveChar(json, 1);
			while ( (json[0] != '"') || (json.size() != 0) )  {	
				if(json[0] == '\\'){
					//control codes!
					//\b\f\n\r\t\u
					switch(json[1]){
					
					case 'b':
						destination.append(1, '\b');
						RemoveChar(json, 2);
						break;
					case 'f':
						destination.append(1, '\f');
						RemoveChar(json, 2);
						break;
					case 'n':
						destination.append(1, '\n');
						RemoveChar(json, 2);
						break;
						
					case 't':
						destination.append(1, '\t');
						RemoveChar(json, 2);
						break;
						
					case 'r':
						destination.append(1, '\r');
						RemoveChar(json, 2);
						break;
						
					
					case 'u':
						destination.append(1, 'u');
						return false;
						//unicode shit here
						RemoveChar(json, 6);
						break;
					case '\\':
						destination.append(1, '\\');
						RemoveChar(json, 2);
						break;
					case '/':
						destination.append(1, '/');
						RemoveChar(json, 2);
						break;
					case '\"':
						destination.append(1, '\"');
						RemoveChar(json, 2);
						break;
						
					
					default:
							return false;
						break;
					}
				} else if(json[0] == '"'){
					//cout << "\n" <<json[0];
					
					RemoveChar(json, 1);
					//done!
					return true;
				} else {
				 //cout << json[0];
					destination.append(1, json[0]); 
					RemoveChar(json, 1);
				}
				
			}
		} else {
		//error
			result = false;
		}
		return result;	
	};
//---
	bool ReadNumber(std::string & json, JNumber & destination){
		double whole=0;
		double decimal=0;
		double exponent=0;
		int sign = 1;
		
		
		//check for sign
		if (json[0]=='+' || json[0]=='-'){
			if (json[0] == '-'){
				sign = -1;
			}
			RemoveChar(json,1);
			TrimWhiteSpace(json);
		
		}
		//
		if (!IsDigit(json[0])) return false;
		//read wholes
		while (IsDigit(json[0])){
			whole = (whole * 10)+(json[0]-'0');
			//cout << json[0] << ":\t" << whole << '\n';
			RemoveChar(json,1);
		}
		
		//if there's a decimal point, read decimals
		if( json[0] == '.'){
			RemoveChar(json,1);
			int fracker = 1;
			
			while(IsDigit(json[0])){
				float fracklet = pow(10,fracker);
				float digit = (json[0] -'0'); 
				decimal += (digit / fracklet);
				//cout << json[0] << ":\t" << digit << '\t' << fracklet << '\t' << decimal <<'\n';
				RemoveChar(json,1);
				++fracker;
			}		
		}
		
		//check for E's. if E read exponent.
		if (json[0] == 'E' || json[0] == 'e'){
			//we have exponents
			RemoveChar(json,1);
			//check for sign if any and remove
			 int expsign = 1;
			 if (json[0] == '+'){
			 	//do nothing
			 	expsign = 1;
			 	RemoveChar(json,1);
			 } else if (json[0] == '-'){
			 	expsign = -1;
			 	RemoveChar(json,1);
			 }
			 //read digits
			 while (IsDigit( json[0])){
			 	exponent = (exponent * 10) + (json[0]-'0');
			 	RemoveChar(json,1);
			 }
			 exponent = exponent * expsign;

		}
		//calc return value

		destination = (	(whole + decimal) * pow(10, exponent) * (sign)	);
		return true;	
	};
//---
	bool ReadBool(std::string & json, JBool & destination){
		JBool result = false;

		std::string tmp = "";

		if (json[0] == 't'){
			if (json.size() < 4) {
				cout << "bounds check error: true \n" ;
				return false;
			}
			//add bounds check here.
			tmp.append(1 , json[0]);
			tmp.append(1 , json[1]);
			tmp.append(1 , json[2]);
			tmp.append(1 , json[3]);
			RemoveChar(json,4);
			//cout << "debug: \t" << (tmp == "true") << "\n";
			if (tmp == "true") {
				result = true;
			} else {
				//error
				cout << "bool read error: True \n" ; 
				return false;
			}

		} else if (json[0] == 'f') {
			//add bounds check here
			if (json.size() < 5) {
				cout << "bounds check error: false \n" ;
				return false;
			}
			tmp.append(1 , json[0]);
			tmp.append(1 , json[1]);
			tmp.append(1 , json[2]);
			tmp.append(1 , json[3]);
			tmp.append(1 , json[4]);
			RemoveChar(json,5);

			if (tmp == "false") {
				result = false;
			} else {
				//error
				cout << "bool read error: false \n" ; 
				return false;
			}	
		}

		destination = result;

		return true;
	};
//---
	bool ReadNull(std::string & json){
		std::string tmp = "";
		if (json[0] == 'n'){
			if (json.size() < 4) {
				cout << "bounds check error: null \n" ;
				return false;
			}
			tmp.append(1 , json[0]);
			tmp.append(1 , json[1]);
			tmp.append(1 , json[2]);
			tmp.append(1 , json[3]);
			RemoveChar(json,4);
			if (tmp == "null"){
				return true;
			} else {
				cout << "null read error";
				return false;
			}
		} else{
			cout << "null read error\n";
			return false;
		}

		return true;
	};
//---
	bool RemoveChar(std::string & json, unsigned int count){
		if (json.size() > count ){
			json = json.substr(count);
		}else{
		 json = "";
		}
	};
//---
	bool IsDigit(JChar Test ){
		//abuse the fact that digits are stored sequentially
		int temp = (Test - '0');
		if (temp >= 0 && temp <= 9 ){
		//is digit
			return true;
		}else{
			return false;
		}
	};

//---
}
//---

};